package ru.socket;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * Created by Dashka on 25.01.2018.
 */
    public class Server {
        public static void main(String[] args) throws IOException {
            ServerSocket serverSocket = new ServerSocket(8080);
            System.out.println("Сервер ждёт клиента....");

            try (Socket clientSocket = serverSocket.accept();
                 DataInputStream input = new DataInputStream(clientSocket.getInputStream());
                 DataOutputStream output = new DataOutputStream(clientSocket.getOutputStream())) {

                System.out.println("Новое соединение: " + clientSocket.getInetAddress().toString());
                String request;
                while ((request = input.readUTF()) != null) {
                    System.out.println("прислал клиент: " + request);
                    output.writeUTF(request);
                    output.flush();
                    if(request == "and"){
                        break;
                    }
                    System.out.println("отправлено клиенту: " + request);
                    Thread.sleep(2000);
                }
                System.out.println("Клиент отключился");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }